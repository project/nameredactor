<?php
/**
 * @file
 * Constructs nameredactor admin form.
 */

/**
 * Build form for admin/settings page.
 */
function nameredactor_settings($form, &$form_state) {
   global $default_bots;

  $form = array();
  $form['nameredactor_general'] = array(
    '#markup' => t('<p>These settings affect all instances of the Name redactor text filter.</p>'),
  );

  $form['nameredactor_redactedstring'] = array(
    '#title' => t('Default text for redacted'),
    '#type' => 'textfield',
    '#description' => t('Enter the default text to use for to avoid displaying personal data.'),
    '#default_value' => variable_get('nameredactor_redactedstring', '[redacted]'),
    '#size' => 32,
    '#maxlength' => 128,
  );
  return system_settings_form($form);
}
