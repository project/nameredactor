## Contents of this file

* [Introduction](#intro)
* [Requirements](#requirements)
* [Recommended modules](#recommended)
* [Installation](#install)
* [Configuration](#config)
* [Performance considerations](#perf)
* [Maintainers](#maintainers)

## Introduction<a id="intro"></a>

This module implements a PET (*Privacy Enhanching Technology*) that
lets the user to hide personal data from search engines.

It is part of *The PET project*.  For more information, visit [The PET
project website][01].

The module works by checking whether the visitor to the site is human
or a search engine robot. If the visitor is a search engine robot, the
module will redact any data marked as personal before delivering the
content.  It will either replace the text with a string set by the
user, or show the string “[redacted]”. To human visitors, the personal
data will appear.

To indicate an instance of personal data the author may use square
brackets to delimit the data, as in the following example:
`[[noindex:John Smith|a man]]`.  The string before the colon
(“noindex”) is known as the *indicator*, and used to distinguish this
filter from other filters that also uses double square brackets as
delimiters. The first string after the colon (“John Smith”) is shown
to humans, the second string after the colon (“a man”) is shown to
search engine robots.

The module is based upon experimental software first described by
Joakim Valla in *[Privacy by Design: Adding Privacy to Publishing
Platforms][02]* (master thesis at the University of Oslo, proposed and
supervised by Gisle Hannemyr &ndash; the creator of this module).

## Requirements<a id="requirements"></a>

* [Advanced help hint][03]:  
  To hint about how to get `README.md` displayed.
* [Robot monitor][04]:  
  To check for robot visit.


## Recommended modules<a id="recommended"></a>

* [Advanced Help][05]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `help/nameredactor/README.md`.
* [Markdown filter][06]:  
  When this module is enabled, the project's `README.md` will be
  rendered with the markdown filter.


## Installation<a id="install"></a>

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][07] for further information.

2. Enable the **Name redactor** module on the *Modules* list page.
   It appears in the “PET” section.

3. Proceed to configure the module as described in the configuraton
   section below.


## Configuration<a id="config"></a>

To alter the global settings for the module, navigate to
*Configuration » Content authoring » Name redactor*.

The only setting is default replacement text for redacted material.

This setting affects all instances of the text filter.

To configure the robot settings, navigate to *Configuration » Web
services » Robot monitor*.

To enable redaction navigate to *Configuration » Content authoring »
Text formats* and select the text format you want to enable redaction
for.  Check the box “Name redactor”.  Repeat this for all formats you
want to enable redaction for.

The **Name redactor** is best suited to appear early in the filter
processing order.

To test that redaction works as expected, you can use the “Fetch as
Google” feature of Google's [*Search console*][08].


## Performance considerations<a id="perf"></a>

This modules turns of caching of nodes where the filter is
enabled. This may impact performance.


## Maintainers<a id="maintainers"></a>

* [gisle][09] - Gisle Hannemyr (creator and current maintainer)

Developement was sponsored by [Hannemyr Nye Medier AS][10].

Any help with development (patches, reviews, comments) are welcome.

[01]: https://pet.roztr.org/
[02]: https://www.duo.uio.no/handle/10852/37434
[03]: https://www.drupal.org/project/advanced_help_hint
[04]: https://www.drupal.org/project/robotmonitor
[05]: https://www.drupal.org/project/advanced_help
[06]: https://www.drupal.org/project/markdown
[07]: https://drupal.org/documentation/install/modules-themes/modules-7
[08]: https://www.google.com/webmasters/tools/home
[09]: https://www.drupal.org/u/gisle
[10]: https://hannemyr.no
